package org.yidu.novel.constant;

public class YiDuConfig {
    // yiduconfig
    public final static String URI = "uri";
    public final static String CONTENT_FILE_PATH = "contentFilePath";
    public final static String SKIP_AUTH_CHECK = "skipAuthCheck";
    public final static String COUNT_PER_PAGE = "countPerPage";
    public final static String CACHE_EFFECTIVE = "cacheEffective";
    public final static String AD_EFFECTIVE = "adEffective";
    public final static String CLEAN_URL = "cleanUrl";
    public final static String FILE_PATH = "filePath";
    public final static String IAMGE_PATH = "iamgePath";
    public final static String RELATIVE_IAMGE_PATH = "relativeIamgePath";
    public final static String MAX_BOOKCASE = "maxBookcase";
    public final static String NUMBER_PER_PAGE = "numberPerPage";
    public final static String GZIP_EFFECTIVE = "gzipEffective";
    public final static String CREATE_INDEXPAGE = "createIndexPage";
    public final static String CREATE_SITEMAP = "createSiteMap";
    public final static String THEME_NAME = "themeName";
    public final static String TXT_ENCODING = "txtEncoding";
    public final static String KEEP_DELETE_DATA_DAYS =  "keepDeleteDataDays";

    // jdbc
    public final static String JDBC_URL = "jdbc.url";
    public final static String JDBC_USERNAME = "jdbc.username";
    public final static String JDBC_PASSWORD = "jdbc.password";
    public final static String JDBC_DBNAME = "jdbc.dbname";
    // language
    public final static String URL = "label.system.url";
    public final static String NAME = "label.system.name";
    public final static String TITLE = "label.system.title";
    public final static String SITEKEYWORDS = "label.system.siteKeywords";
    public final static String SITEDESCRIPTION = "label.system.siteDescription";
    public final static String DOMAIN = "label.system.domain";
    public final static String COPYRIGHT = "label.system.copyright";
    public final static String BEIANNO = "label.system.beianNo";
    public final static String ANALYTICSCODE = "label.system.analyticscode";

}
