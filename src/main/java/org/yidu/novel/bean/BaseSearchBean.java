package org.yidu.novel.bean;

import org.yidu.novel.utils.Pagination;

public class BaseSearchBean {

    private Pagination pagination;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
